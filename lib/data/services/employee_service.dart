import 'package:dartz/dartz.dart';
import 'package:project_flutter/core/model/models.dart';
import 'package:project_flutter/core/network/app_client.dart';
import 'package:project_flutter/resource/api_paths.dart';

import '../data.dart';

class EmployeeService {
  final AppClient _appClient;

  EmployeeService(this._appClient);

  Future<Either<Failure, Employees?>> getEmployees() async {
    final result = await _appClient.get(
      ApiPaths.employees,
      parser: (p0) => Employees.fromJson(p0),
      // queryParameters: {},
    );
    // check for cache here.
    return result;
  }
}
