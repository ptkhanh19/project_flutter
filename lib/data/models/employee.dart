import 'package:json_annotation/json_annotation.dart';

part 'employee.g.dart';

@JsonSerializable()
class Employees {
  final List<Employee?>? employees;

  Employees({this.employees});

  factory Employees.fromJson(Map<String, dynamic> json) =>
      _$EmployeesFromJson(json);
  Map<String, dynamic> toJson() => _$EmployeesToJson(this);
}

@JsonSerializable()
class Employee {
  final String? name;
  final String? avatar;
  final String? address;
  final bool? isMale;

  Employee({this.name, this.avatar, this.address, this.isMale});

  factory Employee.fromJson(Map<String, dynamic> json) =>
      _$EmployeeFromJson(json);
  Map<String, dynamic> toJson() => _$EmployeeToJson(this);
}
