// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'employee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Employees _$EmployeesFromJson(Map<String, dynamic> json) => Employees(
      employees: (json['employees'] as List<dynamic>?)
          ?.map((e) =>
              e == null ? null : Employee.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EmployeesToJson(Employees instance) => <String, dynamic>{
      'employees': instance.employees,
    };

Employee _$EmployeeFromJson(Map<String, dynamic> json) => Employee(
      name: json['name'] as String?,
      avatar: json['avatar'] as String?,
      address: json['address'] as String?,
      isMale: json['isMale'] as bool?,
    );

Map<String, dynamic> _$EmployeeToJson(Employee instance) => <String, dynamic>{
      'name': instance.name,
      'avatar': instance.avatar,
      'address': instance.address,
      'isMale': instance.isMale,
    };
