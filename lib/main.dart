import 'package:flutter/material.dart';
import 'package:project_flutter/resource/enums.dart';
import 'package:project_flutter/resource/route_constants.dart';
import 'package:project_flutter/resource/theme.dart';

import 'bloc/app_bloc.dart';
import 'core/configs/routes_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppBloc().configApp(env: Env.dev);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Demo Flutter',
      theme: AppTheme.get(context),
      onGenerateRoute: RouteConfig.routes,
      onGenerateInitialRoutes: (_) => [
        RouteConfig.routeWithName(routeName: AppRoutes.home),
      ],
    );
  }
}
