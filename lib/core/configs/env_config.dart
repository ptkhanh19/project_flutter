
import 'package:global_configs/global_configs.dart';
import 'package:project_flutter/resource/enums.dart';

class EnvConfig {
  static final EnvConfig _singleton = EnvConfig._internal();

  factory EnvConfig() {
    return _singleton;
  }

  EnvConfig._internal();

  Future<void> load({required Env env}) async {
    final envStr = env.value;
    await GlobalConfigs().loadJsonFromdir('assets/cfg/env_$envStr.json');
  }

  String get apiUrl => GlobalConfigs().get('api_url');
}
