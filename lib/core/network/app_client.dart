import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:project_flutter/core/model/failure.dart';

class AppClient {
  Dio? _dio;

  AppClient({
    required String baseUrl,
    List<Interceptor>? interceptors,
  }) {
    final options = BaseOptions(baseUrl: baseUrl);
    _dio = Dio(options);
    if (interceptors != null && interceptors.isNotEmpty) {
      _dio?.interceptors.addAll(interceptors);
    }
  }

  Future<Either<Failure, T?>> get<T>(
    String path, {
    T? Function(Map<String, dynamic>)? parser,
    Map<String, dynamic>? queryParameters,
  }) async {
    if (_dio == null) {
      return Left(Failure(code: 500, message: 'Dio is null!!!'));
    }
    try {
      final response = await _dio?.get(path, queryParameters: queryParameters);
      return _parseResponse(response, parser: parser);
    } on Exception catch (e) {
      return Left(Failure(code: 500, message: e.toString()));
    } catch (error) {
      return Left(Failure(code: 500, message: error.toString()));
    }
  }

  Future<Either<Failure, T?>> post<T>(
    String path, {
    T? Function(Map<String, dynamic>)? parser,
    dynamic data,
  }) async {
    if (_dio == null) {
      return Left(Failure(code: 500, message: 'Dio is null!!!'));
    }
    try {
      final response = await _dio?.post(path, data: data);
      return _parseResponse(response, parser: parser);
    } on Exception catch (e) {
      return Left(Failure(code: 500, message: e.toString()));
    } catch (error) {
      return Left(Failure(code: 500, message: error.toString()));
    }
  }

  Either<Failure, T?> _parseResponse<T>(
    Response<dynamic>? response, {
    T? Function(Map<String, dynamic>)? parser,
  }) {
    if (response == null) {
      return Left(Failure(code: 500, message: 'Response is null'));
    }
    if (response.statusCode != 200 && response.statusCode != 201) {
      return Left(
          Failure(code: response.statusCode, message: response.statusMessage));
    }
    if (parser == null) {
      return const Right(null);
    }

    final data = response.data['data'] as Map<String, dynamic>?;
    if (data == null) {
      return const Right(null);
    }

    final object = parser.call(data);
    return Right(object);
  }
}
