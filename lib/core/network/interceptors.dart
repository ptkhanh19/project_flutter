import 'package:dio/dio.dart';
import 'dart:developer' as developer;

class AppInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    developer.log('---------------------------------------------------');
    developer.log('REQUEST[${options.method}] => PATH: ${options.path}');
    developer.log('${options.queryParameters}');
    developer.log('${options.data}');
    developer.log('---------------------------------------------------');
    // Add Authorization header here!!!
    // Load Token from cached.
    // options.headers['Authorization'] = 'Bearer ....';
    return super.onRequest(options, handler);
  }

  @override
  Future onResponse(
      Response response, ResponseInterceptorHandler handler) async {
    developer.log('---------------------------------------------------');
    developer.log(
        'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
    developer.log('${response.data}');
    developer.log('---------------------------------------------------');
    return super.onResponse(response, handler);
  }

  @override
  Future onError(DioError err, ErrorInterceptorHandler handler) async {
    developer.log(
        'ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}');
    return super.onError(err, handler);
  }
}
