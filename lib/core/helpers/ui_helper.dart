import 'package:flutter/material.dart';

class UIHelper {
  UIHelper._();

  static const verticalSpace2 = SizedBox(height: 2);
  static const verticalSpace4 = SizedBox(height: 4);
  static const verticalSpace6 = SizedBox(height: 6);
  static const verticalSpace8 = SizedBox(height: 8);
  static const verticalSpace16 = SizedBox(height: 16);
  static const verticalSpace20 = SizedBox(height: 20);
  static const verticalSpace24 = SizedBox(height: 24);
  static const verticalSpace30 = SizedBox(height: 30);
  static const verticalSpace32 = SizedBox(height: 32);
  static const verticalSpace36 = SizedBox(height: 36);
  static const verticalSpace48 = SizedBox(height: 48);
  static const verticalSpace64 = SizedBox(height: 64);

  static const horizontalSpace2 = SizedBox(height: 2);
  static const horizontalSpace4 = SizedBox(height: 4);
  static const horizontalSpace6 = SizedBox(height: 6);
  static const horizontalSpace8 = SizedBox(height: 8);
  static const horizontalSpace16 = SizedBox(height: 16);
  static const horizontalSpace20 = SizedBox(height: 20);
  static const horizontalSpace24 = SizedBox(height: 24);
  static const horizontalSpace30 = SizedBox(height: 30);
  static const horizontalSpace32 = SizedBox(height: 32);
  static const horizontalSpace36 = SizedBox(height: 36);
  static const horizontalSpace48 = SizedBox(height: 48);
  static const horizontalSpace64 = SizedBox(height: 64);
}
