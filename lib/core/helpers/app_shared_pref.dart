
import 'package:project_flutter/resource/pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppSharePref {
  AppSharePref._();

  static Future<bool> _setValue(dynamic value, {required String key}) async{
    if (value == null) {
      return false;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (value is String) {
      return prefs.setString(key, value);
    }else if (value is int) {
      return prefs.setInt(key, value);
    }else if (value is bool) {
      return prefs.setBool(key, value);
    }else if (value is double) {
      return prefs.setDouble(key, value);
    }else if (value is List<String>) {
      return prefs.setStringList(key, value);
    }
    return false;
  }
  
  static Future _getValue({required String key}) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }

  static Future setLocale(String? locale) => _setValue(locale, key: PrefConstants.locale);
  static Future<String?> get getLocale async => await _getValue(key: PrefConstants.locale) as String?;
}