import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:project_flutter/core/helpers/app_shared_pref.dart';
import 'package:project_flutter/resource/locale_constants.dart';

class LocaleHelper {
  LocaleHelper._();

  static List<Locale> get supportedLocales => const [Locale(LocaleConstants.japanese), Locale(LocaleConstants.english)];
  static Locale get fallbackLocale => const Locale(LocaleConstants.japanese);
  

  static Future<Locale> getDefaultLocale() async{
    final cachedLocale = await AppSharePref.getLocale;
    return Locale(cachedLocale ?? LocaleConstants.japanese);
  }

  static Future<void> setDefaultLocale(BuildContext context, {required String localeString}) async{
    final _ = await AppSharePref.setLocale(localeString);
    await context.setLocale(Locale(localeString));
  }
}