import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_flutter/bloc/blocs.dart';
import 'package:project_flutter/core/core.dart';
import 'package:project_flutter/resource/resource.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold<EmployeesBloc>(
      title: const Text('Home'),
      body: Column(
        children: const [
          ExampleWidget(),
          Expanded(child: EmployeesWidget()),
        ],
      ),
      loadData: (bloc) => bloc?.add(OnLoadEmployees()),
    );
  }
}

class ExampleWidget extends StatelessWidget {
  const ExampleWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          const Text(
            "This is Title",
            style: TextStyle(fontSize: 20),
          ),
          UIHelper.verticalSpace4,
          const Text(
            "This is sub text",
            style: TextStyle(color: AppColors.greyTextColor),
          ),
          UIHelper.verticalSpace4,
          ElevatedButton(
            onPressed: () {
              print('Press button');
            },
            child: const Text('Normal button'),
          ),
          UIHelper.verticalSpace4,
          const ElevatedButton(
            onPressed: null,
            child: Text('Disable button'),
          ),
          UIHelper.verticalSpace4,
          TextButton(
            onPressed: () {
              print('Press text button');
            },
            child: const Text('TExt button'),
          ),
          UIHelper.verticalSpace4,
          const TextField(
            decoration: InputDecoration(hintText: 'Nhap thong tin'),
          ),
        ],
      ),
    );
  }
}

class EmployeesWidget extends StatelessWidget {
  const EmployeesWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<EmployeesBloc, EmployeesState>(
      listener: (context, state) {
        if (state is EmployeesFailed) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(state.message ?? ''),
          ));
        }
      },
      builder: (context, state) {
        if (state is EmployeesSuccess) {
          return ListView.builder(
            itemCount: state.employees?.employees?.length ?? 0,
            itemBuilder: (BuildContext context, int index) {
              final item = state.employees?.employees?[index];
              return InkWell(
                onTap: () {
                  // Navigate to detail page.
                  Navigator.of(context)
                      .pushNamed(AppRoutes.detail, arguments: item);
                },
                child: Container(
                  margin: const EdgeInsets.all(8.0),
                  padding: const EdgeInsets.all(16.0),
                  color: AppColors.itemListColor,
                  child: Text(item?.name ?? ''),
                ),
              );
            },
          );
        }
        return const SizedBox();
      },
    );
  }
}
