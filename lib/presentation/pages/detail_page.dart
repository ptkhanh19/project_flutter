import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_flutter/bloc/blocs.dart';
import 'package:project_flutter/core/core.dart';
import 'package:project_flutter/data/data.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold<DetailBloc>(
      title: const Text('detail page'),
      onReveiveArguments: (params, bloc) {
        if (params is Employee) {
          bloc?.add(OnInitDetail(params));
        }
      },
      body: const EmployeeDetailWidget(),
    );
  }
}

class EmployeeDetailWidget extends StatelessWidget {
  const EmployeeDetailWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DetailBloc, DetailState>(
      builder: (context, state) {
        return Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              UIHelper.verticalSpace24,
              Text(state.employee?.name ?? ''),
              UIHelper.verticalSpace8,
              Text(state.employee?.address ?? ''),
            ],
          ),
        );
      },
    );
  }
}
