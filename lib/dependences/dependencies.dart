
import 'package:project_flutter/resource/resource.dart';

import 'external_dependencies.dart';
import 'service_dependencies.dart';
import 'bloc_dependencies.dart';
import 'page_dependencies.dart';

class AppDependencies {
  AppDependencies._();

  static Future<void> config({required Env env}) async {
    await ExternalDependencies.config(env: Env.dev);
    await ServicelDependencies.config();
    await BlocDependencies.config();
    await PageDependencies.config();
  }
}
