import 'package:get_it/get_it.dart';
import 'dart:developer' as dev;

import 'package:project_flutter/data/data.dart';

class ServicelDependencies {
  ServicelDependencies._();

  static Future<void> config() async {
    final getIt = GetIt.instance;
    try {
      getIt.registerLazySingleton<EmployeeService>(
          () => EmployeeService(getIt()));
    } catch (e) {
      dev.log('Config ServicelDependencies failed');
    }
  }
}
