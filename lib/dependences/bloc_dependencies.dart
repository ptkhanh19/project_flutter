import 'package:get_it/get_it.dart';
import 'dart:developer' as dev;

import 'package:project_flutter/bloc/blocs.dart';

class BlocDependencies {
  BlocDependencies._();

  static Future<void> config() async {
    final getIt = GetIt.instance;
    try {
      getIt.registerFactory<EmployeesBloc>(() => EmployeesBloc(getIt()));
      getIt.registerFactory<DetailBloc>(() => DetailBloc());
    } catch (e) {
      dev.log('Config BlocDependencies failed');
    }
  }
}
