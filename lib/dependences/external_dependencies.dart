import 'package:get_it/get_it.dart';
import 'package:project_flutter/core/core.dart';
import 'dart:developer' as dev;

import 'package:project_flutter/resource/resource.dart';

class ExternalDependencies {
  ExternalDependencies._();

  static Future<void> config({required Env env}) async {
    final getIt = GetIt.instance;
    try {
      final appIntereptors = [AppInterceptors()];
      final apiUrl = EnvConfig().apiUrl;

      getIt.registerLazySingleton<AppClient>(() => AppClient(
            baseUrl: apiUrl,
            interceptors: appIntereptors,
          ));
    } catch (e) {
      dev.log('Config ExternalDependencies failed');
    }
  }
}
