import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'dart:developer' as dev;

import 'package:project_flutter/presentation/pages/pages.dart';
import 'package:project_flutter/resource/resource.dart';

class PageDependencies {
  PageDependencies._();

  static Future<void> config() async {
    final getIt = GetIt.instance;
    try {
      getIt.registerFactory<Widget>(() => const HomePage(),
          instanceName: AppRoutes.home);
      getIt.registerFactory<Widget>(() => const DetailPage(),
          instanceName: AppRoutes.detail);
    } catch (e) {
      dev.log('Config PageDependencies failed');
    }
  }
}
