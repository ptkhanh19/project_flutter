import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:project_flutter/data/models/models.dart';

part 'detail_event.dart';
part 'detail_state.dart';

class DetailBloc extends Bloc<DetailEvent, DetailState> {
  DetailBloc() : super(const DetailState()) {
    on<OnInitDetail>((event, emit) {
      emit(state.coppyWith(employee: event.employee));
    });
  }
}
