part of 'detail_bloc.dart';

class DetailState extends Equatable {
  final Employee? employee;

  const DetailState({this.employee});

  DetailState coppyWith({Employee? employee}) {
    return DetailState(employee: employee ?? this.employee);
  }

  @override
  List<Object?> get props => [employee];
}
