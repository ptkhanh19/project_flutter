import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_flutter/data/data.dart';

part 'employees_event.dart';
part 'employees_state.dart';

class EmployeesBloc extends Bloc<EmployeesEvent, EmployeesState> {
  EmployeesBloc(this._employeeService) : super(EmployeesInitial()) {
    on<OnLoadEmployees>(_onLoadEmployees);
  }

  final EmployeeService _employeeService;

  FutureOr<void> _onLoadEmployees(
      OnLoadEmployees event, Emitter<EmployeesState> emit) async {
    final result = await _employeeService.getEmployees();
    final newState = result.fold(
      (failure) => EmployeesFailed(failure.message),
      (employees) => EmployeesSuccess(employees),
    );
    emit(newState);
  }
}
