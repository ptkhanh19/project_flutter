part of 'employees_bloc.dart';

@immutable
abstract class EmployeesState {}

class EmployeesInitial extends EmployeesState {}

class EmployeesSuccess extends EmployeesState with EquatableMixin {
  final Employees? employees;

  EmployeesSuccess(this.employees);

  @override
  List<Object?> get props => [employees];
}

class EmployeesFailed extends EmployeesState {
  final String? message;

  EmployeesFailed(this.message);
}
