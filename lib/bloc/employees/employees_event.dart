part of 'employees_bloc.dart';

@immutable
abstract class EmployeesEvent {}

class OnLoadEmployees extends EmployeesEvent {}
