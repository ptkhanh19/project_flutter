
import 'package:project_flutter/core/configs/env_config.dart';
import 'package:project_flutter/dependences/dependencies.dart';
import 'package:project_flutter/resource/enums.dart';

class AppBloc {
  static final AppBloc _singleton = AppBloc._internal();

  factory AppBloc() {
    return _singleton;
  }

  AppBloc._internal();

  Future<void> configApp({required Env env}) async {
    await EnvConfig().load(env: env);
    await AppDependencies.config(env: env);
  }
}
