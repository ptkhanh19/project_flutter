class UIText {
  UIText._();
  static const hello = 'ui.hello';
}

class MessageText {
  MessageText._();
  static const success = 'msg.success';
}

class ErrorText {
  ErrorText._();
  static const error = 'err.error';
}
