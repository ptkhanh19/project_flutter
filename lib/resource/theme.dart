import 'package:flutter/material.dart';
import 'package:project_flutter/resource/resource.dart';

class AppTheme {
  AppTheme._();

  static final inputBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: AppColors.borderColor),
    borderRadius: BorderRadius.circular(8.0),
  );

  static ThemeData get(BuildContext context) {
    return ThemeData(
      primarySwatch: AppColors.primaryColorSwatch,
      primaryColor: AppColors.primaryColor,
      hintColor: AppColors.greyTextColor,
      inputDecorationTheme: InputDecorationTheme(
        contentPadding: const EdgeInsets.symmetric(horizontal: 16),
        border: inputBorder,
        focusedBorder: inputBorder,
        enabledBorder: inputBorder,
        errorBorder: inputBorder,
      ),
    );
  }
}
