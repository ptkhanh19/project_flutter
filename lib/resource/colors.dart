import 'package:flutter/material.dart';

class AppColors {
  static const backgroundColor = Colors.white;
  static const itemListColor = Colors.black12;
  static const primaryColor = Color(0xFF3B509F);
  static const blackTextColor = Colors.black87;
  static const greyTextColor = Colors.grey;
  static const borderColor = Colors.grey;
  static const disableButtonColor = Colors.grey;

  static const MaterialColor primaryColorSwatch = MaterialColor(
    0xFF3B509F,
    <int, Color>{
      50: Color(0xFF3B509F),
      100: Color(0xFF3B509F),
      200: Color(0xFF3B509F),
      300: Color(0xFF3B509F),
      400: Color(0xFF3B509F),
      500: Color(0xFF3B509F),
      600: Color(0xFF3B509F),
      700: Color(0xFF3B509F),
      800: Color(0xFF3B509F),
      900: Color(0xFF3B509F),
    },
  );
}
